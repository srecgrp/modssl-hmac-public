create database if not exists wp;
grant usage on *.* to wp@localhost identified by 'wp';
grant all privileges on wp.* to wp@localhost;
