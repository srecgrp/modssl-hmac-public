## REQUIREMEENTS

1. [Docker](https://docker.com/) installed

---

## INSTALL

1. Execute the following command(15-20 minutes)

    ```
    $sudo docker-compose up --build
    ```

---

# ACTIVATE WORDPRESS

1. Open a browser and navigate to https://localhost/wordpress/wp-admin/install.php
2. Follow the wordpress installation instructions
3. Login to wordpress admin panel
4. Navigate to Plugins->Installed Plugins
5. Activate mod_ssl hmac plugin

---

# ACTIVATE DRUPAL

1. Open a browser and navigate to https://localhost/drupal
2. Follow the drupal installation instructions(To the verify requirements step click continue anyway)

---