#!/bin/bash
set -e

echo "Creating self signed certificate";
# Create selfsigned Certificate
if [[ -d /var/modssl ]] && [[ ! -e /var/modssl/.cert ]]; then
    cd /var/modssl/;
    echo -e ".\nlocalhost\nlocalhost\nlocalhost\nlocalhost\nlocalhost\nlocalhost\nlocalhost\nlocalhost"  > cert.txt
    openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/ssl/private/apache-selfsigned.key -out /etc/ssl/certs/apache-selfsigned.crt < cert.txt >>  /var/modssl/.logs
    rm cert.txt;
    touch /var/modssl/.cert;
fi
echo "Done self signed certificate";
echo "Downloading and installing apache";
# Install apache with mod_ssl_hmac
if [[ -e /var/modssl/.cert ]] && [[ ! -e /var/modssl/.apache ]]; then
    cd /var/modssl;
    if [[ ! -d /var/modssl/httpd ]]; then
        git clone https://github.com/apache/httpd.git  >>  /var/modssl/.logs;
        sed -i '/#include "mod_ssl.h"/a #include "hasher.h"' /var/modssl/httpd/modules/ssl/mod_ssl.c;
        sed -i '/ssl_io_filter_register(p);/a ap_hook_handler(hasher_handler, NULL, NULL, APR_HOOK_FIRST);' /var/modssl/httpd/modules/ssl/mod_ssl.c;
        sed -i '/mod_ssl.lo dnl/a hasher.lo dnl/' /var/modssl/httpd/modules/ssl/config.m4;
        cp /var/modssl/lib/hasher.c /var/modssl/httpd/modules/ssl/hasher.c;
        cp /var/modssl/lib/hasher.h /var/modssl/httpd/modules/ssl/hasher.h;
        cd /var/modssl/httpd;
        svn co http://svn.apache.org/repos/asf/apr/apr/trunk srclib/apr >>  /var/modssl/.logs;
        ./buildconf >>  /var/modssl/.logs;
        chmod +x configure;
        ./configure --prefix=/usr/local/apache2 >>  /var/modssl/.logs;
    fi

    cp /var/modssl/lib/hasher.c /var/modssl/httpd/modules/ssl/hasher.c;
    cp /var/modssl/lib/hasher.h /var/modssl/httpd/modules/ssl/hasher.h;

    cd /var/modssl/httpd;
    make clean >>  /var/modssl/.logs;
    make >>  /var/modssl/.logs;
    make install >>  /var/modssl/.logs;
    touch /var/modssl/.apache;
fi
echo "Done apache";
echo "Downloading and installing php";
# Install php
if [[ -e /var/modssl/.apache ]] && [[ ! -e /var/modssl/.php ]]; then
    cd /var/modssl/;
    if [[ ! -d /var/modssl/php-7.2.16 ]]; then
        wget -O php.tar.gz http://php.net/get/php-7.2.16.tar.gz/from/this/mirror >>  /var/modssl/.logs;
        tar -zxvf php.tar.gz;
        rm php.tar.gz;
    fi
    cd /var/modssl/php-7.2.16;
    ./configure --with-apxs2=/usr/local/apache2/bin/apxs --with-curl --with-mysqli --enable-mbstring --with-gd --with-jpeg-dir=/usr/lib64 --enable-opcache >>  /var/modssl/.logs;
    make clean >>  /var/modssl/.logs;
    make >>  /var/modssl/.logs;
    make install >>  /var/modssl/.logs;
    cd /var/modssl/;
    touch /var/modssl/.php;
fi
echo "Done php";
echo "Applying apache configuration files";
# Apply configuration files
if [[ -e /var/modssl/.php ]] && [[ ! -e /var/modssl/.conf ]]; then
    cp /var/modssl/conf/httpd.conf /usr/local/apache2/conf/httpd.conf
    cp /var/modssl/conf/httpd-ssl.conf /usr/local/apache2/conf/extra/httpd-ssl.conf
    touch /var/modssl/.conf;
fi

if [[ -e /var/modssl/.started ]]; then
    rm /var/modssl/.started;
fi
# Start mysql and apache
echo "Start mysql and apache";

if [[ -e /var/modssl/.conf ]]; then
    service mysql stop;
    service mysql start;
    /usr/local/apache2/bin/apachectl start;
    touch /var/modssl/.started;
fi
echo "Mysql and apache started";
echo "Creating wordpress database";

# Create wordpress db
if [[ ! -e /var/modssl/.wordpressdbCreated ]] && [[ -e /var/modssl/.started ]]; then
    mysql < /var/modssl/sql/wordpress.sql
    touch /var/modssl/.wordpressdbCreated;
fi
echo "Done wordpress database";
echo "Downloading and installing wordpress";

# Install wordpress with mod_ssl_hmac enabled
if [[ -e /var/modssl/.wordpressdbCreated ]] && [[ ! -e /var/modssl/.wordpress ]]; then
    cd /usr/local/apache2/htdocs;
    wget https://wordpress.org/latest.tar.gz >>  /var/modssl/.logs;
    tar -xzvf latest.tar.gz >>  /var/modssl/.logs;
    rm latest.tar.gz;
    touch /var/modssl/.wordpress;
fi
echo "Done installing wordpress";
echo "Configuring wordpress";
# Configure wordpress
if [[ -e /var/modssl/.wordpress ]] && [[ ! -e /var/modssl/.wordpressConf ]]; then
    mkdir -p /usr/local/apache2/htdocs/wordpress/wp-content/plugins/wp-password-hmac
    cp /var/modssl/module/wp-password-hmac.php /usr/local/apache2/htdocs/wordpress/wp-content/plugins/wp-password-hmac/wp-password-hmac.php
    sed -i 's/CURLOPT_SSL_VERIFYPEER => true/CURLOPT_SSL_VERIFYPEER => false/g' /usr/local/apache2/htdocs/wordpress/wp-content/plugins/wp-password-hmac/wp-password-hmac.php;
    cd /usr/local/apache2/htdocs/wordpress;
    mv wp-config-sample.php wp-config.php;
    sed -i 's/database_name_here/wp/g' wp-config.php;
    sed -i 's/username_here/wp/g' wp-config.php;
    sed -i 's/password_here/wp/g' wp-config.php;
    sed -i 's/localhost/127.0.0.1/g' wp-config.php;
    touch /var/modssl/.wordpressConf;
fi
echo "Done configuring wordpress";
echo "Downloading and installing drupal";
# Install drupal with mod_ssl_hmac enabled
if [[ ! -e /var/modssl/.drupal ]]; then
    cd /usr/local/apache2/htdocs;
    wget https://www.drupal.org/files/projects/drupal-8.6.12.tar.gz >>  /var/modssl/.logs;
    tar -zxvf drupal-8.6.12.tar.gz >>  /var/modssl/.logs;
    rm drupal-8.6.12.tar.gz;
    mv drupal-8.6.12 drupal;
    touch /var/modssl/.drupal;
fi
echo "Done installing drupal";
echo "Configuring drupal";
# Configure drupal
if [[ -e /var/modssl/.drupal ]] && [[ ! -e /var/modssl/.drupalConf ]]; then
    cp /usr/local/apache2/htdocs/drupal/sites/default/default.settings.php /usr/local/apache2/htdocs/drupal/sites/default/settings.php;
    chmod -R a+w /usr/local/apache2/htdocs/drupal/sites/default;
    cd /usr/local/apache2/htdocs/drupal/core/lib/Drupal/Core/Password/;
    patch PhpassHashedPassword.php <  /var/modssl/drupalPatch/drupal.patch;
    sed -i 's/CURLOPT_SSL_VERIFYPEER => true/CURLOPT_SSL_VERIFYPEER => false/g' PhpassHashedPassword.php;
    touch /var/modssl/.drupalConf;
fi
echo "Done configuring drupal";
echo "Container is ready";

cat;
