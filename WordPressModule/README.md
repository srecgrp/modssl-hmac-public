## REQUIREMEENTS

1. Apache httpd server with mod_ssl_hmac ([How to install](../modssl_hmac/README.md))
2. [Wordpress](https://wordpress.com/) installed

---

## INSTALL

1. Create folder wp-password-hmac in your-wordpress-directory/wp-content/plugins/ directory
2. Move wp-password-hmac.php file to your-wordpress-directory/wp-content/plugins/wp-password-hmac/
3. Login to wordpress admin panel
4. Navigate to Plugins->Installed Plugins
5. Activate mod_ssl hmac plugin

---

## CONFIGURE

If you have a self signed certificate

1. Login to wordpress admin panel
2. Navigate to Plugins-> Plugin Editor
3. Select mod_ssl hmac plugin
4. Make the bellow change to the PhpassHashedPassword.php file
    
    Replace
    ```
    CURLOPT_SSL_VERIFYPEER => true
    ```
    
    With
    ```
    CURLOPT_SSL_VERIFYPEER => false
    ```
    
5. Click Update File