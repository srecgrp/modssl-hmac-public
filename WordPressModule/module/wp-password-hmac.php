<?php
/**
 * Plugin Name: Password mod_ssl_hmac
 * Version:     1.0
 * Description: Replaces wp_hash_password and wp_check_password with mod_sll_hmac hashing
 * Plugin URI:  https://bitbucket.org/srecgrp/modsslhmac/
 * Licence:     MIT
 * Author:      Condiom
 * Author URI:  https://bitbucket.org/condiom/
 */

if (! function_exists('add_filter')) {
    header('Status: 403 Forbidden');
    header('HTTP/1.1 403 Forbidden');
    exit();
}

if (version_compare(phpversion(), '5.5', '>=')
    && ! function_exists('wp_check_password')
    && ! function_exists('wp_hash_password')
    && ! function_exists('wp_set_password')
) :

define('WP_OLD_HASH_PREFIX', '$P$');
define('WP_OLD_HASH_REHASHED_PREFIX', '$O$');
define('WP_HMAC_PREFIX', '$N$');

/**
 * Check if user has entered correct password, supports hmac-service and pHash.
 *
 * @param string $password Plaintext password
 * @param string $hash Hash of password
 * @param int|string $user_id ID of user to whom password belongs
 * @return mixed|void
 *
 */
function wp_check_password($password, $hash, $user_id = '')
{

  // Update old passwords that have not been rehashed
    if (0 === strpos($hash, WP_OLD_HASH_PREFIX)) {
        global $wp_hasher;

        if (empty($wp_hasher)) {
            require_once(ABSPATH . WPINC . '/class-phpass.php');

            $wp_hasher = new PasswordHash(8, true);
        }

        $check = $wp_hasher->CheckPassword($password, $hash);
        // If user provided correct password update with the new one
        if ($check && $user_id) {
            // use same salt for backward compatibility
            $hash = wp_set_password(WP_HMAC_PREFIX.substr($hash, 4, 8).$password, $user_id);
        }
    }
    // Update old passwords that have been rehashed
    elseif (0 === strpos($hash, WP_OLD_HASH_REHASHED_PREFIX)) {
        global $wp_hasher;

        if (empty($wp_hasher)) {
            require_once(ABSPATH . WPINC . '/class-phpass.php');

            $wp_hasher = new PasswordHash(8, true);
        }
        $oldSettings = WP_OLD_HASH_PREFIX.substr($hash, 3, 9);
        $oldHash = $wp_hasher->crypt_private($password, $oldSettings);
        $oldHash = substr($oldHash, 12);
        $salt = substr($hash, 4, 8);
        $newHash = wp_hash_password(WP_HMAC_PREFIX.$salt.$oldHash);
        // Check only the hmac part of each one
        $check = (substr($hash, 12)===substr($newHash, 11));
        // If user provided correct password update with the new one
        if ($check && $user_id) {
            // use same salt for backward compatibility
            $hash = wp_set_password(WP_HMAC_PREFIX.$salt.$password, $user_id);
        }
    }
    $check = wp_hash_password(substr($hash, 0, 11).$password) ==  $hash;
    return apply_filters('check_password', $check, $password, $hash, $user_id);
}

/**
 * Hash password using hmac-service
 *
 * @param string $password Plaintext password
 * @return bool|string
 */
 function wp_hash_password($password)
 {
     // Use defined salt salt
     if (0 === strpos($password, WP_HMAC_PREFIX)) {
         $salt = substr($password, 3, 8);
         // remove settings
         $password = substr($password, 11);
     }
     // Create new salt
     else {
         global $wp_hasher;
         if (empty($wp_hasher)) {
             require_once(ABSPATH . WPINC . '/class-phpass.php');
             $wp_hasher = new PasswordHash(8, true);
         }
         $random = $wp_hasher->get_random_bytes(6);
         $salt = $wp_hasher->encode64($random, 6);
     }

     $curl = curl_init();
     curl_setopt_array($curl, array(
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_URL => "https://localhost/hmac-service?password=".urlencode($salt.$password),
        CURLOPT_USERAGENT => 'local',
    // Set to false for a self−signed certificate
        CURLOPT_SSL_VERIFYPEER => true

    ));
     $hash = curl_exec($curl);
     $settings = '$N$'.$salt;
     $hash = $settings.$hash;
     return $hash;
 }

/**
 * Set password using hmac-service
 *
 * @param string $password Plaintext password
 * @param int $user_id ID of user to whom password belongs
 * @return bool|string
 */
function wp_set_password($password, $user_id)
{

    /** @var \wpdb $wpdb */
    global $wpdb;

    $hash = wp_hash_password($password);

    $wpdb->update($wpdb->users, [ 'user_pass' => $hash, 'user_activation_key' => '' ], [ 'ID' => $user_id ]);
    wp_cache_delete($user_id, 'users');

    return $hash;
}

endif;
