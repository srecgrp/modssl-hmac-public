<?php

/**
 * Plugin Name: Password mod_ssl_hmac update old passwords
 * Version:     1.0
 * Description: Updates all old passwords to use mod_ssl_hmac
 * Plugin URI:  https://bitbucket.org/srecgrp/modsslhmac/
 * Licence:     MIT
 * Author:      Condiom
 * Author URI:  https://bitbucket.org/condiom/
 */

 /**
  * Update all passwords runs only once
 */

function update_passwords()
{
    if (get_option('update_passwords_flag') != 'completed') {
        global $wp_hasher;
        global $wpdb;
        if (empty($wp_hasher)) {
            require_once(ABSPATH . WPINC . '/class-phpass.php');
            $wp_hasher = new PasswordHash(8, true);
        }
        // Get all users
        $args = array();
        $args['count_total'] = false;
        $user_query = new WP_User_Query($args);

        if (! empty($user_query->get_results())) {
            // User Loop
            foreach ($user_query->get_results() as $user) {
                $user_id = $user->ID;
                $password  = $user->user_pass;
                $id = substr($password, 0, 3);
                // if password is not old version skip him
                if ($id != '$P$' && $id != '$H$') {
                    continue;
                }
                $salt = substr($password, 4, 8);

                $curl = curl_init();
                curl_setopt_array($curl, array(
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_URL => "https://localhost/hmac-service?password=".urlencode($salt.substr($password, 12)),
                    CURLOPT_USERAGENT => 'local',
                    // Set to false for a self−signed certificate
                    CURLOPT_SSL_VERIFYPEER => true

                ));
                $hash = curl_exec($curl);
                $newSettings = '$O$'.substr($password, 3, 9);
                $hash = $newSettings.$hash;
                $wpdb->update($wpdb->users, [ 'user_pass' => $hash, 'user_activation_key' => '' ], [ 'ID' => $user_id ]);
                wp_cache_delete($user_id, 'users');
            }
        }
        update_option('update_passwords_flag', 'completed');
    }
}

add_action('admin_init', 'update_passwords');
