# modssl_hmac

This repository holds all source code used for the publication:


Practical Password Hardening based on TLS.
Constantinos Diomedous and Elias Athanasopoulos. 
In Proceedings of the 16th Conference on Detection of Intrusions and Malware and Vulnerability Assessment (DIMVA). Gothenburg, Sweden, June 2019.

For more information about DIMVA 2019, please, see: https://www.dimva2019.org
