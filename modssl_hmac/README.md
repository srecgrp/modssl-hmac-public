## INSTALL

1.  Download apache httpd source code([here](https://github.com/apache/httpd))
2.  Move hasher.c and hasher.h files to apacheDirectory/modules/ssl
3.  Navigate to apacheDirectory/modules/ssl/ and execute the following commands
   
    ```$sed -i '/#include "mod_ssl.h"/a #include "hasher.h"' mod_ssl.c;```
    
    ```$sed -i '/ssl_io_filter_register(p);/a ap_hook_handler(hasher_handler, NULL, NULL, APR_HOOK_FIRST);' mod_ssl.c;```
    
    ```$sed -i '/mod_ssl.lo dnl/a hasher.lo dnl/' config.m4;```
    
4.  Navigate to apacheDirectory/ and execute the following commands
    
    ```$svn co <http://svn.apache.org/repos/asf/apr/apr/trunk> srclib/apr;```
    
    ```$./buildconf;```
    
    ```$chmod +x configure;```
    
    ```$./configure --prefix=/usr/local/apache2;```
    
    ```$make clean;```
    
    ```$make;```
    
    ```$make install;```