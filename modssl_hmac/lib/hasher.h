#ifndef __MOD_CONDIOM_H__
#define __MOD_CONDIOM_H__

#include <string.h>
#include <stdlib.h>

#include <openssl/evp.h>
#include <openssl/aes.h>
#include <openssl/err.h>
#include <openssl/sha.h>
#include <openssl/ssl.h>
#include <sys/types.h>
#include <pwd.h>


#include "ssl_private.h"

int hasher_handler(request_rec *r);
int ishex(int x);
int decode(const char *s, char *dec);
int startsWith(const char *a, const char *b);
char* getPasswordFromArgs(char * args);
int getRoundsFromArgs(char * args);
#define PASSWORD_ARGUMENT_HS "password="
#define ROUNDS_ARGUMENT_HS "rounds="
#define PRIVATE_KEY_SIZE 1710

#endif /* __MOD_CONDIOM_H__ */
