#include "hasher.h"


int hasher_handler(request_rec *r)
{
    if(strcmp(r->uri,"/hmac-service")==0 && r->args!=NULL && strcmp(ap_get_remote_host(r->connection, NULL, REMOTE_NAME,NULL),"127.0.0.1")==0){
      char * key;
      server_rec * 	s = r->server;;
      SSLSrvConfigRec *sc = mySrvConfig(s);
      modssl_ctx_t * 	server = sc->server;

      if(server==NULL||server->ssl_ctx==NULL){
        return DECLINED;
      }
      else{
        EVP_PKEY * evp = SSL_CTX_get0_privatekey(server->ssl_ctx);
        if(evp){
          key = malloc(PRIVATE_KEY_SIZE);
          FILE *stringFile = fmemopen(key, PRIVATE_KEY_SIZE, "w");
          PEM_write_PrivateKey(stringFile, evp, NULL, NULL, 0, 0, NULL);
          fclose(stringFile);
        }
        else return DECLINED;
      }
      char * plainPassword = getPasswordFromArgs(r->args);
      int rounds = getRoundsFromArgs(r->args);
      // wrong password format
    	char * dec=malloc(sizeof(char)*strlen(plainPassword)+1);
    	if(plainPassword==NULL  || decode(plainPassword, dec)<0){
        free(dec); free(key);
        return DECLINED;
      }
      int rlen;
      unsigned char * hashed = HMAC(EVP_sha256(), key,strlen(key), dec,strlen(dec),NULL, &rlen);
      int i;
      for(i=1;i<rounds;i++){
        hashed = HMAC(EVP_sha256(), key,strlen(key), hashed,rlen,NULL, &rlen);
      }
      for (i = 0; i < rlen; i++) {
        ap_rprintf(r,"%02X", hashed[i]);
      }

      free(key); free(dec);free(plainPassword);
      return OK;
    }
    // return DECLINED because we will not handle this request
    return DECLINED;
}

char* getPasswordFromArgs(char * args){
  char *arguments = malloc(sizeof(char)*strlen(args)+1);
  strncpy(arguments,args,strlen(args)+1);
  char delim[] = "&";
  char *ptr = strtok(arguments, delim);
  char *plainPassword=NULL;
  while(ptr != NULL)
  {
    if(plainPassword==NULL&&startsWith(ptr,PASSWORD_ARGUMENT_HS)==1){
      int arg_len=strlen(PASSWORD_ARGUMENT_HS);
      plainPassword=malloc(sizeof(char)*strlen(ptr)-arg_len);
      strncpy(plainPassword,ptr+arg_len,strlen(ptr)-arg_len);
      plainPassword[strlen(ptr)-arg_len]='\0';
    }
    ptr = strtok(NULL, delim);
  }
  return plainPassword;
}

int getRoundsFromArgs(char * args){
  char *arguments = malloc(sizeof(char)*strlen(args)+1);
  strncpy(arguments,args,strlen(args)+1);
  char delim[] = "&";
  char *ptr = strtok(arguments, delim);
  int rounds = 0;
  while(ptr != NULL)
  {
    if(rounds==0&&startsWith(ptr,ROUNDS_ARGUMENT_HS)==1){
      rounds = atoi(ptr+strlen(ROUNDS_ARGUMENT_HS));
    }
    ptr = strtok(NULL, delim);
  }
  if(rounds==0){
    rounds=1;
  }
  return rounds;
}

int startsWith(const char *a, const char *b)
{
   if(strncmp(a, b, strlen(b)) == 0) return 1;
   return 0;
}

int ishex(int x)
{
	return	(x >= '0' && x <= '9')	||
		(x >= 'a' && x <= 'f')	||
		(x >= 'A' && x <= 'F');
}

int decode(const char *s, char *dec)
{
	char *o;
	const char *end = s + strlen(s);
	int c;

	for (o = dec; s <= end; o++) {
		c = *s++;
		if (c == '+') c = ' ';
		else if (c == '%' && (	!ishex(*s++)	||
					!ishex(*s++)	||
					!sscanf(s - 2, "%2x", &c)))
			return -1;

		if (dec) *o = c;
	}

	return o - dec;
}
