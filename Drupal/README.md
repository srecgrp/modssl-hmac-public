## REQUIREMEENTS

1. Apache httpd server with mod_ssl_hmac ([How to install](../modssl_hmac/README.md))
2. [Drupal](https://www.drupal.org/) installed

---

## INSTALL

1. Move drupal.patch file to your-drupal-directory/core/lib/Drupal/Core/Password/;
2. Navigate to your-drupal-directory/core/lib/Drupal/Core/Password/
3. Execute the following command

    ```$patch PhpassHashedPassword.php <  drupal.patch;
    ```

---

## CONFIGURE

If you have a self signed certificate

1. Navigate to your-drupal-directory/core/lib/Drupal/Core/Password/
2. Make the bellow change to the PhpassHashedPassword.php file
    
    Replace
    ```
    CURLOPT_SSL_VERIFYPEER => true
    ```
    
    With
    ```
    CURLOPT_SSL_VERIFYPEER => false
    ```
